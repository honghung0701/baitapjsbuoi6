// Bài tập 1:
document.getElementById('soNhoNhat').onclick = function() {
    var sum = 0;
    var n = 0;
    while (sum < 10000) {
        n++;
        sum = sum + n;
    }
    document.getElementById('ketQuaBai1').innerHTML = `Số nguyên dương nhỏ nhất là: ` + n;
}
// Bài tập 2:
document.getElementById('sum').onclick = function() {
    var n = document.getElementById('n').value*1;
    var x = document.getElementById('x').value*1;
    var S = 0;
    var T = 0;
    for (var i = 1; i <= n; i++) {
        T = Math.pow(x,i);
        S = S + T;
    }
    document.getElementById('ketQuaBai2').innerHTML = `Tổng : ` + S;
}
// Bài tập 3:
document.getElementById('tinh').onclick = function() {
    var N = document.getElementById('nhapN').value*1;
    var giaiThua = 1;
    for (var i = 1; i <= N; i++) {
        giaiThua = giaiThua * i;
    }
    document.getElementById('ketQuaBai3').innerHTML = `Giai thừa: ` + giaiThua;
}
// Bài tập 4:
document.getElementById('createDiv').onclick = function() {
    var content ='';
    for (var x = 1 ; x <= 10; x++) {
        if ( x % 2 == 0) {
            var div = `<div class="alert alert-warning bg-danger">Div chẵn</div>`;
            content = content + div;
        } else {
            var div = `<div class="alert alert-warning bg-primary">Div lẻ</div>`;
            content = content + div;
        }
    }
    document.getElementById('ketQuaBai4').innerHTML = content;
}

// Bài tập 5:
document.getElementById('soNguyenTo').onclick = function() {
    var a = document.getElementById('a').value*1;
    var result =''
    for (var i = 2; i <= a; i++) {
        if (i % 2 != 0 && i % Math.sqrt(i) != 0) {
            result = result + ' ' + i ;
        } else {
            result = result;
        }
    }
    document.getElementById('ketQuaBai5').innerHTML = result;
}